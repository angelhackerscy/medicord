from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json
import urlparse
import threading
import time
import pymongo
import uuid
from processor import Processor


class BaseThread(threading.Thread):
    def __init__(self, target_args=None, callback=None, callback_args=None, *args, **kwargs):
        target = kwargs.pop('target')
        super(BaseThread, self).__init__(target=self.target_with_callback, *args, **kwargs)
        self.callback = callback
        self.method = target
        self.target_args = target_args
        self.callback_args = callback_args

    def target_with_callback(self):
        self.method(self.target_args)
        if self.callback is not None:
            self.callback(self.callback_args)


class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        # Client requests the results of a job ID. Return status and results
        params = urlparse.parse_qs(urlparse.urlsplit(self.path).query)
        self._set_headers()
        status, result =  self.check_job(params['id'][0])
        response = {'status': status, 'result': result}
        self.wfile.write(json.dumps(response))

    def do_POST(self):
        # Client submits csv, return the Job ID
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        path = urlparse.urlparse(self.path).path.split('/')[1]
        print path
        if path == "upload-file":
            self._set_headers()
            job_id = self.create_job(post_data)
            response = {'id' : job_id}
            self.wfile.write(json.dumps(response))
        if path == "questionnaire":
            print "here"

    def check_job(self, job_id):
        # Check if job is still running, if not, return results
        if job_id not in threads.keys():
            return False, 'ERROR: Wrong Job ID'
        if threads[job_id].isAlive():
            return False, 'PENDING'
        #TODO: Return Results

    def create_job(self, data):
        print data

        # Create job id and assign it to the job
        job_id = str(uuid.uuid4())
        thread = BaseThread(
            name=job_id,
            target=self.parse_data,
            target_args=(data, job_id),
            callback=self.cb,
            callback_args=job_id
        )
        thread.start()
        threads[job_id] = thread
        return job_id

    def parse_data(self, args):
        # Job for parsing data
        data, jobId = args
        print "Parsing data %s" % (data)
        print "jobId %s" % (jobId)

        processor = Processor(data, jobId)
        analyzedData = processor.analyze()
        #time.sleep(15)
        print "parsing ended successfully!"
        print "analysed Data: "
        print analyzedData

    def cb(self, job_id):
        # Callback of Job parsing
        print "Job {} is done".format(job_id)
        del threads[job_id]
        # TODO add results somewere

    def questionnaire(self, data):
        mongo = self.mongoconnection()
        usersquestionnaire = mongo.usersQuestionnaire

        usersquestionnaire.insert(data)

    def mongoconnection(self):
        connection = pymongo.Connection("mongodb://localhost", safe=True)
        return connection

def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()


if __name__ == "__main__":
    from sys import argv
    threads = dict()
    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()