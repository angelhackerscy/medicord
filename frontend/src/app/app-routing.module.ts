import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { LoginComponent } from './components/login/login.component'
import { SignupComponent } from './components/signup/signup.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { QuestionnaireComponent } from './components/questionnaire/questionnaire.component'

const routes: Routes = [

  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'questionnaire', component: QuestionnaireComponent},
  {path: '**', redirectTo: '/login'}

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
