import { Component, OnInit } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dragOver: boolean
  uploading = false
  processing = false

  results: any
  resultsId: string
  resultsRetry: 3

  constructor (private httpClient: HttpClient) {
    this.resultsId = localStorage.getItem('results-id')

    if (this.resultsId) {
      this.resultsRetry = 3
      this.getResults()
    }
  }

  ngOnInit () {}

  onDragOver (e) {
    e.preventDefault()
    return true
  }

  onDrop (e) {
    this.onFileSelect(e.dataTransfer)
  }

  onFileSelect (input: HTMLInputElement) {
    const files = input.files

    if (files && files.length) {
      const fileToRead = files[0]
      this.uploading = true

      const fileReader = new FileReader()
      fileReader.onload = (fileLoadedEvent: any) => {
        const textFromFileLoaded = fileLoadedEvent.target.result
        this.httpClient
          .post('http://medicord.xyz:8888/upload-file', textFromFileLoaded)
          .subscribe(
            (response: any) => {
              this.uploading = false
              this.resultsId = response.id
              this.resultsRetry = 3
              this.getResults()
            },
            (error: HttpErrorResponse) => {
              this.uploading = false
              console.error(error)
            }
          )
      }

      fileReader.readAsText(fileToRead, 'UTF-8')
    }
  }

  getResults () {
    this.results = null
    this.processing = true

    setTimeout(() => {
      this.httpClient
        .get('http://medicord.xyz:8888/job?id=' + this.resultsId)
        .subscribe(
          (response: any) => {
            this.results = response

            if (response.status === 'done') {
              return this.processing = false
            }

            setTimeout(() => this.getResults(), 3000)
          },
          error => {
            if (this.resultsRetry-- === 0) {
              return this.processing = false
            }

            setTimeout(() => this.getResults(), 2000)
          })
    }, 5000)
  }
}
