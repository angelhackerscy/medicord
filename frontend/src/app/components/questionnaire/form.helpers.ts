import { generateTimeRange } from './time.helper'

export const SessionDropdownLists = {
  bowelMovementChoices: [
    {
      value: 'normal',
      viewValue: 'Normal'
    },
    {
      value: 'constipation',
      viewValue: 'Constipation'
    },
    {
      value: 'diarrhea',
      viewValue: 'Diarrhea'
    },
    {
      value: 'irregular',
      viewValue: 'Irregular'
    }
  ],
  sleepQualityChoices: [
    {
      value: 'lessThanFiveHours',
      viewValue: 'Less than 5 hours/night'
    },
    {
      value: 'aboutFiveHours',
      viewValue: 'About 5 hours/night'
    },
    {
      value: 'aboutSixHours',
      viewValue: 'About 6 hours/night'
    },
    {
      value: 'aboutSevenHours',
      viewValue: 'About 7 hours/night'
    },
    {
      value: 'aboutEightHours',
      viewValue: 'About 8 hours/night'
    },
    {
      value: 'aboutNineHours',
      viewValue: 'About 9 hours/night'
    },
    {
      value: 'aboutTenHours',
      viewValue: 'About 10 hours/night'
    },
    {
      value: 'moreThanTenHours',
      viewValue: 'More than 10 hours/night'
    }
  ],
  physicalActivityChoices: [
    {
      value: 'sedentary',
      viewValue: 'Sedentary'
    },
    {
      value: 'light',
      viewValue: 'Light'
    },
    {
      value: 'moderate',
      viewValue: 'Moderate'
    },
    {
      value: 'heavy',
      viewValue: 'Heavy'
    },
    {
      value: 'veryHeavy',
      viewValue: 'Very Heavy'
    }
  ],
  smokerChoices: [
    {
      value: 'yes',
      viewValue: 'Yes'
    },
    {
      value: 'no',
      viewValue: 'No'
    }
  ],
  alcoholConsumptionChoices: [
    {
      value: 'yes',
      viewValue: 'Yes'
    },
    {
      value: 'no',
      viewValue: 'No'
    }
  ],
  maritalStatusChoices: [
    {
      value: 'single',
      viewValue: 'Single'
    },
    {
      value: 'married',
      viewValue: 'Married'
    },
    {
      value: 'divorced',
      viewValue: 'Divorced'
    },
    {
      value: 'widower',
      viewValue: 'Widower'
    }
  ],
  raceChoices: [
    {
      value: 'caucasian',
      viewValue: 'Caucasian'
    },
    {
      value: 'black',
      viewValue: 'Black'
    },
    {
      value: 'asian',
      viewValue: 'Asian'
    }
  ],
  usualWakeUpTimeChoices: generateTimeRange({ from: 1, to: 24, returnObjects: true }),
  usualBedTimeChoices: generateTimeRange({ from: 1, to: 24, returnObjects: true }),
  waterIntakeChoices: [
    {
      value: '<0.5',
      viewValue: 'Less than 0.5 litres'
    },
    {
      value: '0.5-1.0',
      viewValue: 'Between 0.5 and 1 litre'
    },
    {
      value: '1-1.5',
      viewValue: 'Between 1 and 1.5 litre'
    },
    {
      value: '1.5-2.0',
      viewValue: 'Between 1.5 and 2 litre'
    },
    {
      value: '2.0-2.5',
      viewValue: 'Between 2.0 and 2.5 litre'
    },
    {
      value: '2.5-3.0',
      viewValue: 'Between 2.5 and 3.0 litre'
    },
    {
      value: '>3.0',
      viewValue: 'More than 3 litres'
    },

  ]
}
