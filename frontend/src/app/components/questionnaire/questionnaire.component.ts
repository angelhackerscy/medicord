import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'

import { HttpClient } from '@angular/common/http'

import { MatSnackBar } from '@angular/material'

import { Router } from '@angular/router'

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe'
import { SessionDropdownLists } from './form.helpers'

@AutoUnsubscribe()
@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {

  public informationAndHabitsForm: FormGroup
  public measurementsAndCalculationsForm: FormGroup

  public dropdownLists = SessionDropdownLists

  constructor (public snackBar: MatSnackBar,
               private fb: FormBuilder,
               private http: HttpClient,
               private router: Router) {}

  ngOnInit () {
    this.informationAndHabitsForm = this.fb.group({
      personalAndSocialHistory: this.fb.group({
        age: [''],
        height: [''],
        weight: [''],
        country: [''],
        bowelMovements: [''],
        usualWakeUpTime: [''],
        usualBedTime: [''],
        sleepQuality: [''],
        physicalActivity: [''],
        smoker: [''],
        alcoholConsumption: [''],
        maritalStatus: [''],
        race: [''],
        otherInformation: ['']
      }),
      medicalHistory: this.fb.group({
        diseases: [''],
        medication: [''],
        personalHistory: [''],
        familyHistory: [''],
        otherInformation: ['']
      }),
      dietaryHistory: this.fb.group({
        favoriteFood: [''],
        dislikedFood: [''],
        allergiesAndIntolerances: [''],
        waterIntake: [''],
        otherInformation: ['']
      })
    })
  }

  onSubmit () {
    this.http
      .post('http://medicord.xyz:8888/', this.informationAndHabitsForm.value)
      .subscribe(
        res => {
          console.log(res)
          this.snackBar.open('Thank you, your data has been recorded', '', {
            duration: 3000,
          })
          this.router.navigate(['/'])
        },
        error => {
          console.error(error)
        }
      )
  }

  ngOnDestroy (): void {
    // this needs to be defined for AOT even though the decorator AutoUnsubscribe is in place
  }

}
