export function generateTimeRange (opts) {
  // {
  // from: { time: 5 },
  // to: { time: 21 }
  // }
  const timeRange = []
  const startingTime = opts.from
  const endingTime = opts.to

  const loops = endingTime - startingTime

  for (let i = 0; i < loops + 1; i++) {
    const hours = startingTime + i > 12
      ? startingTime + i === 24
        ? '00'
        : startingTime + i - 12
      : startingTime + i

    const amPm = startingTime + i < 12 || startingTime + i === 24
      ? 'am'
      : 'pm'

    const time00 = `${hours}:00 ${amPm}`
    const time15 = `${hours}:15 ${amPm}`
    const time30 = `${hours}:30 ${amPm}`

    if (opts.returnObjects) {
      timeRange.push({ value: time00, viewValue: time00 }, { value: time15, viewValue: time15 }, { value: time30, viewValue: time30 })
    } else {
      timeRange.push(time00, time15, time30)
    }
  }

  return timeRange
}