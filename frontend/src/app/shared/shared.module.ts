import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HttpClientModule } from '@angular/common/http'

import { FlexLayoutModule } from '@angular/flex-layout'

import { MaterialFrameworkModule } from './material.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

const modules = [
  FormsModule,
  CommonModule,
  HttpClientModule,
  FlexLayoutModule,
  HttpClientModule,
  ReactiveFormsModule,
  MaterialFrameworkModule,
]

@NgModule({
  imports: modules,
  exports: modules,
  providers: [],
  declarations: [],
  entryComponents: []
})
export class SharedModule {}
